package com.sv_soft.svcommon;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.PopupMenu;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by Sergii Volchenko on 15.07.16.
 * sergey@volchenko.com
 * made in Ukraine
 */
public class AppCommon {
    /**
     * Check application permission
     *
     * @param permission - permission name
     * @param context    - Context
     * @return true - permission granted or false otherwise.
     */
    public static boolean checkPermission(String permission, Context context) {
        int res = context.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static Bitmap scaleDown(String file, float maxImageSize,
                                   boolean filter) {

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file, opt);
        opt.inJustDecodeBounds = false;

        if (Math.max(opt.outHeight, opt.outWidth) < maxImageSize) {
            return BitmapFactory.decodeFile(file, opt);
        }
        int ratio = (int) Math.max(
                (int) opt.outHeight / maxImageSize,
                (int) opt.outWidth / maxImageSize);
        opt.inSampleSize = ratio;
        return BitmapFactory.decodeFile(file, opt);
    }

    /**
     * Scaledown image
     *
     * @param uri          - Uri to image
     * @param maxImageSize - image size
     * @param context      - Context
     * @return Bitmap with scaled image or null if image not found
     */
    public static Bitmap scaleDown(Uri uri, float maxImageSize,
                                   Context context) {


        InputStream stream = null;
        try {
            stream = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            return null;
        }
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(stream, null, opt);
        opt.inJustDecodeBounds = false;

        if (Math.max(opt.outHeight, opt.outWidth) < maxImageSize) {
            return BitmapFactory.decodeStream(stream, null, opt);
        }
        int ratio = (int) Math.max(
                (int) opt.outHeight / maxImageSize,
                (int) opt.outWidth / maxImageSize);
        opt.inSampleSize = ratio;
        try {
            stream = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            return null;
        }
        return BitmapFactory.decodeStream(stream, null, opt);
    }

    /**
     * Copy stream to file
     *
     * @param in   - InputStream
     * @param file - file
     * @throws Exception
     */
    public static void copyInputStreamToFile(InputStream in, File file) throws Exception {

        OutputStream out = new FileOutputStream(file);
        byte[] buf = new byte[4 * 1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        out.close();
        in.close();

    }

    /**
     * Close cursor if it opened
     *
     * @param cur - cursor
     */
    public static void cursorClose(Cursor cur) {
        if (cur != null && !cur.isClosed()) {
            cur.close();
        }
    }


    /**
     * Hide soft keyboard
     *
     * @param ctx  - Context
     * @param view - curent view
     */
    public static void hideKeyboard(Context ctx, View view) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Show soft keyboard
     *
     * @param ctx  - Context
     * @param view - current view
     */
    public static void showKeyboard(Context ctx, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * Return screen width
     *
     * @param activity - Activity
     * @return - width in pixels
     */
    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    /**
     * Translate DP to Pixels
     *
     * @param ctx - Context
     * @param dp  - DP
     * @return Pixels
     */
    public static float dpToPx(Context ctx, float dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, ctx.getResources().getDisplayMetrics());
    }

    /**
     * Translate DP to Pixels
     *
     * @param ctx - Context
     * @param px  - Pixels
     * @return Android DP
     */
    public static float pxToDp(Context ctx, float px) {
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;

    }

    /**
     * Close SQLiteDatabase if it opened
     *
     * @param db - SQLite database
     */
    public static void dbClose(SQLiteDatabase db) {
        if (db != null && db.isOpen()) {
            db.close();
        }
    }

    /**
     * Write message to Log only in debug mode with predefined Log tag
     *
     * @param msg
     */
    public static void logV(String msg) {
        logV("w201", msg);
    }

    /**
     * Write message to Log only in debug mode
     *
     * @param tag - Log tag
     * @param msg - message
     */
    public static void logV(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg);
        }
    }

    /**
     * Get View by possition in ListView
     *
     * @param pos      - position in a listview
     * @param listView - ListView
     * @return View of element in position pos
     */
    public static View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    /**
     * Check is valid email or not
     *
     * @param target - email
     * @return true if email is valid
     */
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    /**
     * Scale down image
     *
     * @param realImage    - image source
     * @param maxImageSize - maximum of weight or height
     * @param filter       - apply filter or not
     * @return resized image or source image if error ocure
     */
    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        Bitmap newBitmap = null;
        try {
            float ratio = Math.min(
                    (float) maxImageSize / realImage.getWidth(),
                    (float) maxImageSize / realImage.getHeight());
            int width = Math.round((float) ratio * realImage.getWidth());
            int height = Math.round((float) ratio * realImage.getHeight());

            newBitmap = Bitmap.createScaledBitmap(realImage, width,
                    height, filter);
            return newBitmap;

        } catch (Exception e) {
            return realImage;
        }
    }

    /**
     * Set height of list view based on items on screen Useful if your listview height setup before listview
     * item created
     *
     * @param listView - Listview
     * @return - true if operation success
     */
    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }

    /**
     * Get Status bar height
     *
     * @param ctx - Context
     * @return - status bar height in pixels
     */
    public static int getStatusBarHeight(Context ctx) {
        int result = 0;
        int resourceId = ctx.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = ctx.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Get file extension
     *
     * @param name - Filename
     * @return - file extension
     */
    public static String getFileExtension(String name) {
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * File copy
     *
     * @param src - Source file
     * @param dst - Destination file
     * @throws IOException
     */
    public static void fileCopy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    /**
     * Validate is URL valid or not
     *
     * @param url - URL
     * @return true if url is valid
     */
    public static boolean isValidURL(String url) {

        URL u = null;
        try {
            u = new URL(url);
        } catch (MalformedURLException e) {
            return false;
        }

        try {
            u.toURI();
        } catch (URISyntaxException e) {
            return false;
        }

        return true;
    }

    /**
     * Show popup menu for certain view
     *
     * @param ctx               - Context
     * @param v                 - View
     * @param menu              - Menu ID
     * @param onItemClick       - OnItemClickListener
     * @param onDismissListener - OnDismissMenuListener
     * @return Popup Menu
     */
    public static PopupMenu showPopupMenu(Context ctx, View v, int menu, PopupMenu.OnMenuItemClickListener onItemClick,
                                          PopupMenu.OnDismissListener onDismissListener) {
        PopupMenu popupMenu = new PopupMenu(ctx, v);

        try {
            Field[] fields = popupMenu.getClass().getDeclaredFields();
            for (Field field : fields) {
                if ("mPopup".equals(field.getName())) {
                    field.setAccessible(true);
                    Object menuPopupHelper = field.get(popupMenu);
                    Class<?> classPopupHelper = Class.forName(menuPopupHelper
                            .getClass().getName());
                    Method setForceIcons = classPopupHelper.getMethod(
                            "setForceShowIcon", boolean.class);
                    setForceIcons.invoke(menuPopupHelper, true);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= 18) {
            popupMenu.inflate(menu); // Для Android 4.0
        } else {
            popupMenu.getMenuInflater().inflate(menu,
                    popupMenu.getMenu());
        }

        popupMenu
                .setOnMenuItemClickListener(onItemClick);

        popupMenu.setOnDismissListener(onDismissListener);
        popupMenu.show();
        return popupMenu;
    }

    public static Drawable getDrawableById(Context context, String name) {
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(name, "drawable",
                context.getPackageName());
        return resources.getDrawable(resourceId);
    }
}
